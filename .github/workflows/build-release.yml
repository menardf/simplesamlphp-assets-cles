---
name: Build assets for SimpleSAMLphp 2.1

on:  # yamllint disable-line rule:truthy
  push:
    branches: ['release-*']
  pull_request:
    branches: ['*']
  workflow_dispatch:

jobs:
  quality:
    name: Quality checks
    runs-on: ['ubuntu-latest']

    steps:
      - uses: actions/checkout@v3
        with:
          token: ${{ secrets.ASSETS_COMMIT_TOKEN }}
          ref: ${{ github.head_ref || github.ref_name }}
          # Full git history is needed to get a proper list of changed files within `super-linter`
          fetch-depth: 0

      - name: Fetch changes
        # Without fetching, we might miss new tags due to caching in Github Actions
        run: git fetch --all

      - name: Remove generated files
        # Remove generated files; we can't lint them and we want to detect removed files during build
        run: find css/ fonts/ icons/ js/ -type f -not -name '.gitkeep' -delete

      - name: Lint Code Base
        uses: super-linter/super-linter/slim@v5
        env:
          VALIDATE_ALL_CODEBASE: true
          LINTER_RULES_PATH: 'tools/linters'
          VALIDATE_CSS: true
          VALIDATE_JAVASCRIPT_ES: true
          VALIDATE_JSON: true
          VALIDATE_MARKDOWN: true
          VALIDATE_YAML: true
          VALIDATE_GITHUB_ACTIONS: true
          DEFAULT_BRANCH: ${{ github.head_ref || github.ref_name }}
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
          # CSS Linter will choke on generated assets
          IGNORE_GENERATED_FILES: true
          FILTER_REGEX_EXCLUDE: '(css|js|fonts|icons)/.*'

      - name: Zip artifact for deployment
        run: |
          zip release.zip -r .

      - uses: actions/upload-artifact@v3
        with:
          name: release
          path: release.zip
          retention-days: 1

  build:
    name: Build assets
    runs-on: ['ubuntu-latest']
    needs: quality

    outputs:
      files_changed: ${{ steps.changes.outputs.files_changed }}

    steps:
      - uses: actions/setup-node@v3
        with:
          node-version: 18

      - uses: actions/download-artifact@v3
        with:
          name: release

      - name: unzip artifact for deployment
        run: |
          unzip release.zip
          rm release.zip

      - name: Install & build assets
        run: |
          # Make sure the lock-file is up to date before we run clean-install
          npm install --package-lock-only
          npm clean-install
          npm audit fix
          npx browserslist@latest --update-db
          npm run build
          # Remove artifact from build-process that should not be published
          rm js/stylesheet.js js/stylesheet.js.map
          rm js/postSubmit.js js/postSubmit.js.map

      - name: Fix git safe.directory in container
        run: |
          mkdir -p /home/runner/work/_temp/_github_home
          printf "[safe]\n\tdirectory = /github/workspace" > /home/runner/work/_temp/_github_home/.gitconfig

      - name: Diff the changes after building
        shell: pwsh
        # Give an id to the step, so we can reference it later
        id: changes
        run: |
          git add --all
          $Diff = git diff --cached --name-only

          # Check if a file under css/ js/ icons/ or fonts/ that has changed (added, modified, deleted)
          $SourceDiff = $Diff | Where-Object {
              $_ -match '^css/' -or
              $_ -match '^fonts/' -or
              $_ -match '^icons/' -or
              $_ -match '^js/'
          }
          echo "Changed files"
          echo $SourceDiff

          $HasSourceDiff = $SourceDiff.Length -gt 0
          echo "($($SourceDiff.Length) changes)"
          echo "files_changed=$HasSourceDiff" >> $env:GITHUB_OUTPUT

      - name: Zip artifact for deployment
        if: steps.changes.outputs.files_changed == 'true' || steps.changes.outputs.packages_changed
        run: zip build.zip -r .

      - uses: actions/upload-artifact@v3
        if: steps.changes.outputs.files_changed == 'true' || steps.changes.outputs.packages_changed
        with:
          name: build
          path: build.zip
          retention-days: 1

  version:
    name: Determine next version
    runs-on: ['ubuntu-latest']
    needs: build
    if: needs.build.outputs.files_changed == 'true' && github.event_name == 'push'

    outputs:
      version: ${{ steps.asset_version.outputs.version }}

    steps:
      - uses: actions/download-artifact@v3
        with:
          name: build

      - name: unzip artifact for deployment
        run: |
          unzip build.zip
          rm build.zip

      - uses: paulhatch/semantic-version@v5.0.3
        id: asset_version
        with:
          # The prefix to use to identify tags
          tag_prefix: "v"

          # A string which, if present in a git commit, indicates that a change represents a
          # major (breaking) change, supports regular expressions wrapped with '/'
          major_pattern: "(MAJOR)"

          # Same as above except indicating a minor change, supports regular expressions wrapped with '/'
          minor_pattern: "(MINOR)"

          # A string to determine the format of the version output
          version_format: "${major}.${minor}.${patch}"

          # Optional path to check for changes. If any changes are detected in the path the
          # 'changed' output will true. Enter multiple paths separated by spaces.
          change_path: "css/** js/** fonts/** icons/** package.json package-lock.json"

  commit:
    name: Commit changes to assets
    needs: version
    runs-on: [ubuntu-latest]

    steps:
      - uses: actions/download-artifact@v3
        with:
          name: build

      - name: unzip artifact for deployment
        run: |
          unzip build.zip
          rm build.zip

      - name: Add & Commit
        uses: EndBug/add-and-commit@v9
        with:
          # The arguments for the `git add` command (see the paragraph below for more info)
          # Default: '.'
          add: "['resources/*', 'css/*', 'fonts/*', 'icons/*', 'js/*']"

          # Determines the way the action fills missing author name and email. Three options are available:
          # - github_actor -> UserName <UserName@users.noreply.github.com>
          # - user_info -> Your Display Name <your-actual@email.com>
          # - github_actions -> github-actions <email associated with the github logo>
          # Default: github_actor
          default_author: github_actions

          # The message for the commit.
          # Default: 'Commit from GitHub Actions (name of the workflow)'
          message: "[skip ci] Auto-rebuild assets"

          # The way the action should handle pathspec errors from the add and remove commands.
          # Three options are available:
          # - ignore -> errors will be logged but the step won't fail
          # - exitImmediately -> the action will stop right away, and the step will fail
          # - exitAtEnd -> the action will go on, every pathspec error will be logged at the end, the step will fail.
          # Default: ignore
          pathspec_error_handling: exitImmediately

          # Arguments for the git tag command (the tag name always needs to be the first word not preceded by an hyphen)
          # Default: ''
          tag: "v${{ needs.version.outputs.version }}"

  cleanup:
    name: Cleanup artifacts
    needs: [build, commit]
    runs-on: [ubuntu-latest]
    if: |
      always() &&
      needs.commit.result == 'success' ||
      (needs.build.result == 'success' && needs.commit.result == 'skipped')

    steps:
      - uses: geekyeggo/delete-artifact@v2
        with:
          name: |
            release
            build
